<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Tableau de bord</title>

    <!-- Fontfaces CSS-->
    <link href="<?php echo base_url();?>assertion/css/font-face.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url();?>assertion/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url();?>assertion/vendor/font-awesome/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url();?>assertion/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="<?php echo base_url();?>assertion/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="<?php echo base_url();?>assertion/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url();?>assertion/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url();?>assertion/vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url();?>assertion/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <!--<link href="vendor/slick/slick.css" rel="stylesheet" media="all">-->
    <link href="<?php echo base_url();?>assertion/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url();?>assertion/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="<?php echo base_url();?>assertion/css/theme.css" rel="stylesheet" media="all">

</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <header class="header-mobile d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        <li class="has-sub">
                            <a href="dashboard">
                                <i class="fas fa-tachometer-alt"></i>Dashboard
							</a>
                    
                        </li>
                        
                    </ul>
                </div>
            </nav>
        </header>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar d-none d-lg-block">
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <li class="active has-sub">
                            <a class="js-arrow" href="dashboard">
                                <i class="fas fa-tachometer-alt"></i>Dashboard
							</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            <form class="form-header" action="" method="POST">
                                <input class="au-input au-input--xl" type="text" name="search" placeholder="Search for datas &amp; reports..." />
                                <button class="au-btn--submit" type="submit">
                                    <i class="zmdi zmdi-search"></i>
                                </button>
                            </form>
                            <div class="header-button">
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                            <div class="account-dropdown__footer">
                                                <a href="deconnection">
                                                    <i class="zmdi zmdi-power">Logout</i>
												</a>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- HEADER DESKTOP-->

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row m-t-25">
                            
                            <div class="col-sm-6 col-lg-3">
                                <div class="overview-item overview-item--c2">
                                    <div class="overview__inner">
                                        <div class="overview-box clearfix">
                                            <div class="icon">
                                                <i class="zmdi zmdi-shopping-cart"></i>
                                            </div>
                                            <div class="text">
                                                <span>frequence de vente par mois</span>
                                            </div>
                                        </div>
                                        <div class="overview-chart">
                                            <canvas id="widgetChart2"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                <div class="overview-item overview-item--c3">
                                    <div class="overview__inner">
                                        <div class="overview-box clearfix">
                                            <div class="icon">
                                                <i class="zmdi zmdi-calendar-note"></i>
                                            </div>
                                            <div class="text">
                                                <span>frequence de vente journaliere</span>
                                            </div>
                                        </div>
                                        <div class="overview-chart">
                                            <canvas id="widgetChart3"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-9">
                                <h2 class="title-1 m-b-25">Top 5 des repas les plus apprecier</h2>
                                <div class="table-responsive table--no-card m-b-40">
                                    <table class="table table-borderless table-striped table-earning">
                                        <thead>
                                            <tr>
                                                <th>Nom du plat</th>
                                                <th>quantite vendue</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										<?php for($a=0; $a<count($Milay); $a++){?>
                                            <tr>
                                                <td><?php echo $Milay[$a]['NOMPLAT'] ;?></td>
                                                <td class="text-right"><?php echo $Milay[$a]['QTE'];?></td>
                                            </tr>
										<?php }?>
                                    </table>
                                </div>
                            </div>
                            <div class="col-lg-3">
                            <h2 class="title-1 m-b-25">repas moins appreciers</h2>
                                <div class="au-card au-card--bg-blue au-card-top-countries m-b-40">
                                    <div class="au-card-inner">
                                        <div class="table-responsive">
                                            <table class="table table-top-countries">
                                                <tbody>
												<?php for($a=0; $a<count($Milay); $a++){?>
													<tr>
														<td><?php echo $TsyMilay[$a]['NOMPLAT'] ;?></td>
													</tr>
												<?php }?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                    <p>
									Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.
									</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>

    </div>

    <!-- Jquery JS-->
    <script src="<?php echo base_url();?>assertion/vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="<?php echo base_url();?>assertion/vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="<?php echo base_url();?>assertion/vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="<?php echo base_url();?>assertion/vendor/slick/slick.min.js">
    </script>
    <script src="<?php echo base_url();?>assertion/vendor/wow/wow.min.js"></script>
    <script src="<?php echo base_url();?>assertion/vendor/animsition/animsition.min.js"></script>
    <script src="<?php echo base_url();?>assertion/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="<?php echo base_url();?>assertion/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="<?php echo base_url();?>assertion/vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="<?php echo base_url();?>assertion/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="<?php echo base_url();?>assertion/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="<?php echo base_url();?>assertion/vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="<?php echo base_url();?>assertion/vendor/select2/select2.min.js">
    </script>

	<!-- Main JS manpandeh-->
	
	<script>
		function getData(){
			var tab = {listDate:[], listMontant:[]};
			<?php for($a=0 ; $a< count($FrequenceMensuel) ; $a++){ ?>
                tab.listDate.push( (<?php echo $FrequenceMensuel[$a]['annee'];?>).toString()+"-"+(<?php echo $FrequenceMensuel[$a]['mois'] ;?>).toString() );
                console.log(<?php echo $a; ?>);
				tab.listMontant.push(<?php echo $FrequenceMensuel[$a]['qte']; ?>);
			<?php }?>
			return tab;
		}
		getData();
		
		function getData2(){
			var tab = {listDate2:[], listMontant2:[]};
			<?php for($a=0 ; $a< count($FrequenceJournaliaire); $a++){ ?>
                <?php
                    $parti = explode("-", $FrequenceJournaliaire[$a]['DATECOMMANDE']) ;
                ?>
                tab.listDate2.push( (<?php echo $parti[0] ;?>).toString()+"-"+(<?php echo $parti[1] ;?>).toString()+"-"+(<?php echo $parti[2] ;?>).toString());
				tab.listMontant2.push( <?php echo $FrequenceJournaliaire[$a]['qte'];?>);
				
            <?php } ?>
			// alert(tab.listDate2.length);
			return tab;
		}
		getData2();
    </script>
    
    <!-- Main JS-->
    <script src="<?php echo base_url();?>assertion/js/main.js"></script>

</body>
</html>
<!-- end document-->