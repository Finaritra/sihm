<table class = "table table-bordered">
   <thead>
      <tr>
         <td>Plat commandé</td>
         <td>N° table</td>
         <td>Prix unitaire</td>
         <td>Quantité</td>
         <td>Sous-total</td>
         <td>Options</td>
      </tr>
   </thead>
   <tbody>
   <?php foreach ($commandes as $items){ ?>
      <tr>
         <td><?php echo $items["name"]; ?></td>
         <td class="text text-right"><?php echo $items["table"]; ?></td>
         <td class="text text-right"><?php echo number_format($items["price"], 0); ?></td>
         <td class="text text-right"><?php echo $items["qty"]; ?></td>
         <td class="text text-right"><?php echo number_format($items["price"]*$items["qty"], 0); ?></td>
         <td><a href="delete?id=<?php echo $items["id"]; ?>"><button class="btn btn-warning">Annuler</button></a>
         <form action = "ajout" method="post">
            <input type="hidden" name="idEmp" value="<?php echo $_SESSION['AVATAR'];?>">
            <input type="hidden" name="idTable" value="<?php echo $items["table"]; ?>">
            <input type="hidden" name="idPlat" value="<?php echo $items["id"]; ?>">
            <input type="hidden" name="qte" value="<?php echo $items["qty"]; ?>">
            <button class="btn btn-success">Valider</button></td>
         </form>
      </tr>
   <?php } ?>
   </tbody>
</table>
<button></button>