		<!-- Entrees -->
                <div class="col-lg-4 themenu_column">
					<div class="themenu_col">
						<div class="themenu_col_title">Entrées</div>
						<div class="dish_list">

                            <?php for($i=0; $i<count($entrees); $i++){ ?>

							<div class="dish">
								<div class="dish_title_container d-flex flex-xl-row flex-column align-items-start justify-content-start">
									<div class="dish_title"><?php echo $entrees[$i]['NOMPLAT']; ?></div>
									<div class="dish_price"><?php echo number_format($entrees[$i]['PRIX'], 0); ?> Ariary</div>
								</div>
								<div class="dish_contents">
									<ul class="d-flex flex-row align-items-start justify-content-start flex-wrap">
									
									</ul>
								</div>
								<div class="dish_order"><a href="commander?idplat=<?php echo $entrees[$i]['IDPLAT']; ?>">Ajouter commande</a></div>
									
							</div>

                            <?php } ?>
						</div>
					</div>
				</div>

		<!-- Résistance -->
				<div class="col-lg-4 themenu_column">
					<div class="themenu_col">
						<div class="themenu_col_title">Résistance</div>
						<div class="dish_list">
							
                            <?php for($i=0; $i<count($resistances); $i++){ ?>

							<div class="dish">
								<div class="dish_title_container d-flex flex-xl-row flex-column align-items-start justify-content-start">
									<div class="dish_title"><?php echo $resistances[$i]['NOMPLAT']; ?></div>
									<div class="dish_price"><?php echo number_format($resistances[$i]['PRIX'], 0); ?> Ariary</div>
								</div>
								<div class="dish_contents">
									<ul class="d-flex flex-row align-items-start justify-content-start flex-wrap">
										
									</ul>
								</div>
								<div class="dish_order"><a href="commander?idplat=<?php echo $resistances[$i]['IDPLAT']; ?>">Ajouter commande</a></div>
							</div>

                            <?php } ?>
						</div>
					</div>
				</div>

        <!-- Desserts -->
				<div class="col-lg-4 themenu_column">
					<div class="themenu_col">
						<div class="themenu_col_title">Desserts</div>
						<div class="dish_list">

                            <?php for($i=0; $i<count($desserts); $i++){ ?>
                            
							<div class="dish">
								<div class="dish_title_container d-flex flex-xl-row flex-column align-items-start justify-content-start">
									<div class="dish_title"><?php echo $desserts[$i]['NOMPLAT']; ?></div>
									<div class="dish_price"><?php echo number_format($desserts[$i]['PRIX'], 0); ?> Ariary</div>
								</div>
								<div class="dish_contents">
									<ul class="d-flex flex-row align-items-start justify-content-start flex-wrap">
										
									</ul>
								</div>
								<div class="dish_order"><a href="commander?idplat=<?php echo $desserts[$i]['IDPLAT']; ?>">Ajouter commande</a></div>
							</div>

                            <?php } ?>
						</div>
					    </div>
			    </div>