<?php

class Home_Controller extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->library('session');
	}

	public function index()
	{
		$this->load->database();
		$data;
		$this->load->model('plats_model');

		$starters = $this->plats_model->select2(" WHERE IDCATEGORIE = 1");
		$data['platsEntree'] = $starters;

		$main = $this->plats_model->select2(" WHERE IDCATEGORIE = 2");
		$data['platsResistance'] = $main;

		$deserts = $this->plats_model->select2(" WHERE IDCATEGORIE = 3");
		$data['platsDesserts'] = $deserts;
		
		$this->load->helper('url'); 
		$this->load->view('template', $data);
	}		
	
}

?>
