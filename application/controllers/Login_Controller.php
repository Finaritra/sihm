<?php
class Login_Controller extends CI_Controller {
	
	function __construct(){
		parent::__construct();
	}
	
	public function index()
	{
		$this->load->view('backOfficeLogin_View');	
	}
	public function login(){
        $this->load->model('Employe_Model');
        $this->load->helper('url');
        $this->load->library('session');
		
		$login = $this->input->post("login");
		$password = $this->input->post("motdepasse");
		
		$where = " where LOGIN like '%s' and MOTDEPASSE = SHA1('%s')";
		$where = sprintf($where, $login, $password);
		$result = $this->Employe_Model->select2($where);
		
		if(count($result) == 0){
			redirect('', 'refresh');
		}else{
			$_SESSION['AVATAR'] = $result[0]['IDEMPLOYE'];
			redirect('home', 'refresh');
		}
	}

}

?>
