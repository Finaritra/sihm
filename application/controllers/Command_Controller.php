<?php
class Command_Controller extends CI_Controller{

	function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
    }
    
    public function pushInCart(){
       
        $this->load->model('Plats_Model');
        $plats = $this->Plats_Model->select2(" WHERE idPlat = ".$this->input->post('idPlat'));
        $cartItem = array(
            'id' => $this->input->post('idPlat'),
            'qty' => $this->input->post('quantity'),
            'price' => $this->input->post('price'),
            'table' => $this->input->post('tableId'),
            'name' => $plats[0]['NOMPLAT'],
            'photo' => $plats[0]['IMAGE']
        );
        $this->cart->insert($cartItem);
        redirect('commandes', 'refresh');
    }

    public function avoirCommandes(){
        $cartContents = $this->cart->contents();
        $data['commandes'] = $cartContents;
        $data['vue'] = 'CommandList_View.php';
        $data['av'] = $_SESSION['AVATAR'];
        $this->load->view('Template', $data);
    }

    public function delFromCart($idProduit){
		
		$deleteItem = array(
			'IDPRODUIT' => $idProduit,
			'qty' => 0
		);
		
        $this->cart->update($deleteItem);
        redirect('home', 'refresh');
    }
    public function moveFromCart(){
        $idProduit = $this->input->get('id');
        $this->delFromCart($idProduit);
    }

    public function addCommand(){
        $idPlat = $this->input->post('idPlat');
        $idEmpl = $this->input->post('idEmp');
        $idTable = $this->input->post('idTable');
        $qte = $this->input->post('qte');
		
		$this->load->model('Commandes_Model');
		$date = $this->Commandes_Model->getDateNow();
		
		$COMMANDE = array(
			'IDCOMMANDE' => null,
			'IDEMPLOYE' => $idEmpl,
			'IDTABLES' => $idTable,
			'IDPLAT' => $idPlat,
			'QTE' => $qte,
			'DATECOMMANDE' => $date[0]['now']
		);
		
		$this->Commandes_Model->insert($COMMANDE);

        $this->delFromCart($idPlat);
    }

    public function nonFacture(){
        $query = $this->db->query("select datecommande, idtables, sum(qte) as qte, prix, nomplat from detailcommande where idcommande not in (select idCommande from facture) group by datecommande, idtables;");
        $data['commande'] = $query->result_array();
        $data['vue'] = 'ImpayesVue.php';
        $this->load->view('Template', $data);
    }
    
}
?>