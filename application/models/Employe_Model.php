<?php

class Employe_Model extends CI_Model{

	function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
	public function insert($employe) { 
        if ($this->db->insert("EMPLOYE", $employe)) { 
           return true; 
        } 
    } 
   
    public function delete($idemploye) { 
        if ($this->db->delete("EMPLOYE", "IDEMPLOYE = ".$idemploye)) { 
           return true; 
        } 
    } 
   
    public function update($employe, $idemploye) { 
        $this->db->set($employe); 
        $this->db->where("IDEMPLOYE", $idemploye); 
        $this->db->update("EMPLOYE", $employe); 
    }
	
	public function select($idemploye){
		$resultat = $this->db->select('*')->from('EMPLOYE')->where("IDEMPLOYE", $idemploye)->get()->result();
		return $resultat;
	}
	
	public function select2($where){
		$query = $this->db->query("select * from EMPLOYE ".$where);
        $resultat = $query->result_array();
        return $resultat;
	}
}
?>