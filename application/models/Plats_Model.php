<?php

class Plats_Model extends CI_Model{

	function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
	public function insert($plats) { 
        if ($this->db->insert("PLATS", $plats)) { 
           return true; 
        } 
    } 
   
    public function delete($idplat) { 
        if ($this->db->delete("PLATS", "IDPLAT = ".$idplat)) { 
           return true; 
        } 
    } 
   
    public function update($plats, $idplat) { 
        $this->db->set($plats); 
        $this->db->where("IDPLAT", $idplat); 
        $this->db->update("PLATS", $plats); 
    }
	
	public function select($idplat){
		$resultat = $this->db->select('*')->from('PLATS')->where("IDPLAT", $idplat)->get()->result();
		return $resultat;
	}
	
	public function select2($where){
		$query = $this->db->query("select * from PLATS ".$where);
		$resultat = $query->result_array();
		return $resultat;
	}
}
?>