<?php

class Roles_Model extends CI_Model{

	function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
	public function insert($role) { 
        if ($this->db->insert("ROLES", $role)) { 
           return true; 
        } 
    } 
   
    public function delete($idrole) { 
        if ($this->db->delete("ROLES", "IDROLE = ".$idrole)) { 
           return true; 
        } 
    } 
   
    public function update($role, $idrole) { 
        $this->db->set($role); 
        $this->db->where("IDROLE", $idrole); 
        $this->db->update("ROLES", $role); 
    }
	
	public function select($idrole){
		$resultat = $this->db->select(*)->from('ROLES')->where("IDROLE", $idrole)->get()->result();
		return $resultat;
	}
	
	public function select2($where){
		$query = $this->db->query("select * from ROLES ".$where);
		$resultat = $query->result_array();
	}
}
?>